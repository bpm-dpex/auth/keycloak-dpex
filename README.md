# Keycloak Theme for DPEX Framework

This repository contains a custom Keycloak theme tailored for the DPEX Framework. Additionally, it includes a Docker image pre-configured with this theme, which can be utilized in projects via Docker Compose.

## Features

- **Custom Keycloak Theme**: Developed using the Keycloakify project, this theme allows for converting a React app into a Keycloak theme. The design elements are sourced from the DPEX-React Project and adapted to fit the Keycloakify setup.
- **Docker Image**: The Docker image (`registry.gitlab.com/bpm-dpex/auth/keycloak-dpex:keycloak`) is available and can be included in projects by referencing it in a Docker Compose file. This facilitates easy integration and deployment.
- **BPM ID Registration**: Enables the registration of BPM IDs when creating new users.

## Development

### Prerequisites

- Yarn
- Maven (Note: Windows systems cannot convert the project to a JAR archive for the theme)

### Setup

1. Clone the repository.
2. Install dependencies:
   ```bash
   yarn
   ```

### Run in Development

To view and develop the theme:

```bash
yarn storybook
```

### Build

To convert the theme into the final JAR file for deployment on Keycloak (only supported on Linux/Mac):

```bash
yarn build-keycloak-theme
```

## Theme Structure

- **Theme Pages**: Located in `/src/keycloak-theme/pages`.
- **Registration**: Pages are registered in the `src/KCApp.tsx` file.
- **Base Component**: The base component `Template.tsx` is fundamental to the theme.
- **Storybook Tests**: Create your own test scenarios in Storybook by adding files in `/src/keycloak-theme/pages` named like `Pagename.stories.tsx`.

## Additional Resources

- [Keycloakify Project on GitHub](https://github.com/keycloakify/keycloakify)
- [Starter Project](https://github.com/keycloakify/keycloakify#starter-project) - This project serves as the foundation for developing the custom theme.
- For detailed guidance on theme development, refer to the documentation of Keycloakify and the Starter Project.
