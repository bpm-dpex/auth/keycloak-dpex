import { PUBLIC_URL } from 'PUBLIC_URL';
import { Navbar, Container, Image, Nav, Button, Dropdown } from 'react-bootstrap';
import { IoMdExit } from "react-icons/io";
import dpexLogoUrl from "./assets/dpex-logo.png";

interface UrlType {
    loginAction: string;
    resourcesPath: string;
    resourcesCommonPath: string;
    loginRestartFlowUrl: string;
    loginUrl: string;
}

function NavBar(props: UrlType) {
    return (
        <Navbar style={navbarStyle}>
            <Container fluid>
                <Navbar.Brand>
                    <a href={props.loginUrl}>
                        <Image src={dpexLogoUrl} style={brandStyle} />
                    </a>
                </Navbar.Brand>
                <>
                    <Nav className='justify-content-start' style={titleStyle}>
                        User Management
                    </Nav>
                    <Nav className='me-auto'>
                    </Nav>
                    <Nav className='justify-content-end'>
                        <Button href='http://localhost:3000/dpex-login' className='navbar-btn'><IoMdExit size={25} color="white"></IoMdExit></Button>
                    </Nav>
                </>
            </Container>
        </Navbar>
    );
}

export default NavBar;

const brandStyle = {
    height: 30
}

const navbarStyle = {
    background: '#3F3F3F',
    border: 'none'
}

const titleStyle = {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 20 // Adjust the value as needed
}
