import { Col, Container, Row } from "react-bootstrap"

function Footer(){
    return(
        <Container fluid style={footer}>
            <Row className='justify-content-center'>
                <Col className='col-auto'>
                    <span style={{color: 'white', fontWeight: 'bolder'}}>Versions</span>
                </Col>
                <Col className='col-auto'>
                    <Row>
                        <span style={footerItem}><a style={footerItem} href='https://quay.io/repository/keycloak/keycloak' target='_blank' rel='noreferrer'>Keycloak</a>@23.0</span>
                    </Row>
                </Col>
            </Row>
        </Container>
    )
}

export default Footer

const footerItem = {
    color: 'white'
}

const footer = {
    marginTop: 10,
    background: '#3f3f3f'
}