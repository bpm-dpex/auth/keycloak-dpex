import { useState, type FormEventHandler } from "react";
import { clsx } from "keycloakify/tools/clsx";
import { useConstCallback } from "keycloakify/tools/useConstCallback";
import type { PageProps } from "keycloakify/login/pages/PageProps";
import { useGetClassName } from "keycloakify/login/lib/useGetClassName";
import type { KcContext } from "../kcContext";
import type { I18n } from "../i18n";
import { Row, Col, Button, Form, Container } from "react-bootstrap";

const my_custom_param = new URL(window.location.href).searchParams.get("my_custom_param");

if (my_custom_param !== null) {
    console.log("my_custom_param:", my_custom_param);
}

export default function Login(props: PageProps<Extract<KcContext, { pageId: "login.ftl" }>, I18n>) {
    const { kcContext, i18n, doUseDefaultCss, Template, classes } = props;

    const { getClassName } = useGetClassName({
        doUseDefaultCss,
        classes
    });

    const { social, realm, url, usernameHidden, login, auth, registrationDisabled } = kcContext;

    const { msg, msgStr } = i18n;

    const [isLoginButtonDisabled, setIsLoginButtonDisabled] = useState(false);

    const onSubmit = useConstCallback<FormEventHandler<HTMLFormElement>>(e => {
        e.preventDefault();
        setIsLoginButtonDisabled(true);
        const formElement = e.target as HTMLFormElement;
        //NOTE: Even if when login with email is activated Keycloak expects username and password in
        //the POST request.
        console.log(formElement)
        formElement.querySelector("input[name='email']")?.setAttribute("name", "username");
        formElement.querySelector("input[name='usernameOrEmail']")?.setAttribute("name", "username");
        console.log(formElement)
        formElement.submit();
    });

    return (
        <>
            <Template
                {...{ kcContext, i18n, doUseDefaultCss, classes }}
                displayInfo={
                    false
                }
                displayWide={realm.password && social.providers !== undefined}
                headerNode={msg("doLogIn")}
                infoNode={
                    // This will not be displayed as registration in this React Bootstrap Implementation is handled by the login form
                    <div id="kc-registration">
                        <span>
                            {msg("noAccount")}
                            <a tabIndex={6} href={url.registrationUrl}>
                                {msg("doRegister")}
                            </a>
                        </span>
                    </div>
                }
            >
                {/* Removed keycloakify starter wrapper */}
                {realm.password && (
                    <>
                        <Row>
                            <Col>
                                <Form id="kc-form-login" onSubmit={onSubmit} action={url.loginAction} method="post">
                                    {!usernameHidden && (
                                        <Form.Group controlId="formBasicUsername" className="mb-3">
                                            <Form.Label>
                                                {realm.loginWithEmailAllowed
                                                    ? realm.registrationEmailAsUsername
                                                        ? msg("email")
                                                        : msg("usernameOrEmail")
                                                    : msg("username")}
                                            </Form.Label>
                                            <Form.Control
                                                type={realm.loginWithEmailAllowed && realm.registrationEmailAsUsername ? "email" : "text"}
                                                placeholder={msg(realm.loginWithEmailAllowed ? realm.registrationEmailAsUsername ? "email" : "usernameOrEmail" : "username").props.children}
                                                name={realm.loginWithEmailAllowed ? realm.registrationEmailAsUsername ? "email" : "usernameOrEmail" : "username"}
                                                defaultValue={login.username ?? ""}
                                                autoFocus={true}
                                                autoComplete={"off"}
                                            />
                                        </Form.Group>
                                    )}
                                    <Form.Group controlId="formBasicPassword" className="mb-3">
                                        <Form.Label>{msg("password")}</Form.Label>
                                        <Form.Control type="password" name="password" placeholder={msg("password").props.children} />
                                        {realm.resetPasswordAllowed && (
                                            <a href={url.loginResetCredentialsUrl} style={{ textDecoration: 'none' }}>
                                                <Form.Text style={{ width: '100%', color: 'darkblue' }} >
                                                    {msg("doForgotPassword")}
                                                </Form.Text>
                                            </a>
                                        )}
                                    </Form.Group>

                                    {realm.rememberMe && !usernameHidden && (
                                        <Form.Group controlId="rememberMe" className="mb-3">
                                            <Form.Check type="checkbox" name="rememberMe" label={msg("rememberMe")} defaultChecked={login.rememberMe === "on"} />
                                        </Form.Group>
                                    )}

                                    <Container>
                                        {realm.password && realm.registrationAllowed && !registrationDisabled && (
                                            <Row>
                                                <Col className="justify-content-start" style={{ marginTop: "0px" }}>
                                                    <Form.Text>
                                                        {msg("noAccount")}
                                                    </Form.Text>
                                                </Col>
                                            </Row>
                                        )}
                                        <Row>
                                            {realm.password && realm.registrationAllowed && !registrationDisabled && (
                                                <Col>
                                                    <Button
                                                        variant="secondary"
                                                        style={loginButton}
                                                        onClick={() => {
                                                            window.location.href = url.registrationUrl;
                                                        }}
                                                    >{msg("doRegister")}</Button>
                                                </Col>
                                            )}

                                            <Col>
                                                <Form.Group style={{ margin: "0px", padding: "0px" }}>
                                                    {/* Hidden input for credential ID */}
                                                    <Form.Control style={{ margin: "0px", padding: "0px" }}
                                                        type="hidden"
                                                        id="id-hidden-input"
                                                        name="credentialId"
                                                        value={auth?.selectedCredential || ''}
                                                    />

                                                    {/* Submit button */}
                                                    <Button
                                                        variant="primary"
                                                        type="submit"
                                                        name="login"
                                                        id="kc-login"
                                                        disabled={isLoginButtonDisabled}
                                                        style={loginButton}
                                                    >
                                                        {msgStr("doLogIn")}
                                                    </Button>
                                                </Form.Group>
                                            </Col>
                                        </Row>
                                    </Container>
                                </Form>

                                {/* Code not changed from keycloakify starter project as sso currently not supported by dpex */}
                                {realm.password && social.providers !== undefined && (
                                    <div
                                        id="kc-social-providers"
                                        className={clsx(getClassName("kcFormSocialAccountContentClass"), getClassName("kcFormSocialAccountClass"))}
                                    >
                                        <ul
                                            className={clsx(
                                                getClassName("kcFormSocialAccountListClass"),
                                                social.providers.length > 4 && getClassName("kcFormSocialAccountDoubleListClass")
                                            )}
                                        >
                                            {social.providers.map(p => (
                                                <li key={p.providerId} className={getClassName("kcFormSocialAccountListLinkClass")}>
                                                    <a href={p.loginUrl} id={`zocial-${p.alias}`} className={clsx("zocial", p.providerId)}>
                                                        <span>{p.displayName}</span>
                                                    </a>
                                                </li>
                                            ))}
                                        </ul>
                                    </div>
                                )}
                            </Col>
                        </Row>
                    </>
                )}
            </Template>
        </>
    );
}


const loginButton = {
    width: '100%',
    marginBottom: 25
}