import { ComponentStory, ComponentMeta } from '@storybook/react';
import { createPageStory } from "../createPageStory";

const { PageStory } = createPageStory({
    pageId: "register.ftl"
});

export default {
    title: "login/Register",
    component: PageStory,
} as ComponentMeta<typeof PageStory>;

export const Default: ComponentStory<typeof PageStory> = () => (
    <PageStory
        kcContext={{
        }}
    />
);


// Mock data for kcContext and i18n
const mockKcContext = {
    messagesPerField: {
      printIfExists: (field: string, className: any) => field === "firstName" ? className : undefined
    },
    register: {
      formData: {}
    },
    realm: {
      registrationEmailAsUsername: false,
    },
    passwordRequired: false
  };
  
  const mockI18n = {
    msg: (key: any) => key,
    msgStr: (key: any) => key
  };
  
  // Define the default story for the Register component
  export const TestError: ComponentStory<typeof PageStory> = () => (
    <PageStory
      kcContext={{passwordRequired: false}}
      // Include other props as necessary
    />
  );