import { Container, Row, Col, Button, Form } from "react-bootstrap";
import type { PageProps } from "keycloakify/login/pages/PageProps";
import type { KcContext } from "../kcContext";
import type { I18n } from "../i18n";
import Template from "../Template";

export default function LoginResetPassword({
    kcContext,
    i18n,
}: PageProps<Extract<KcContext, { pageId: "login-reset-password.ftl" }>, I18n>) {
    const { url, realm } = kcContext;


    return (
        <Template
            kcContext={kcContext} i18n={i18n} doUseDefaultCss={false} headerNode={i18n.msg("emailForgotTitle")}>
            <Container>
                <Row>
                    <Col >
                        <Form id="kc-reset-password-form" action={url.loginAction} method="post">
                            <Form.Group className="mb-3">
                                <Form.Label htmlFor="username">
                                    {!realm.loginWithEmailAllowed
                                        ? i18n.msg("username")
                                        : !realm.registrationEmailAsUsername
                                            ? i18n.msg("usernameOrEmail")
                                            : i18n.msg("email")}
                                </Form.Label>
                                <Form.Control type="text" id="username" name="username" placeholder={!realm.loginWithEmailAllowed ? i18n.msg("username").props.children : !realm.registrationEmailAsUsername ? i18n.msg("usernameOrEmail").props.children : i18n.msg("email").props.children} autoFocus />
                            </Form.Group>
                            <Container>
                                <Row>
                                    <Col>
                                        <Button variant="secondary" href={url.loginUrl} style={loginButton}>
                                            {i18n.msg("backToLogin")}
                                        </Button>
                                    </Col>
                                    <Col>
                                        <Button variant="primary" type="submit" style={loginButton}>
                                            {i18n.msgStr("doSubmit")}
                                        </Button>

                                    </Col>
                                </Row>
                            </Container>
                        </Form>
                    </Col>
                </Row>
            </Container>
        </Template>
    );
}

const loginButton = {
    width: '100%',
    marginBottom: 25,
    marginTop: 15
}