// ejected using 'npx eject-keycloak-page'
import type { PageProps } from "keycloakify/login/pages/PageProps";
import { useGetClassName } from "keycloakify/login/lib/useGetClassName";
import type { KcContext } from "../kcContext";
import type { I18n } from "../i18n";
import { Container, Form, Button, Col, Row, Dropdown } from "react-bootstrap";
import { SetStateAction, useState } from "react";
import { RiCloseLine } from 'react-icons/ri';
import { IoAddSharp } from "react-icons/io5";

// It was first thought for matrix and discord ID to be included in the user management, since this is no longer the case they are commented out but can be used as an example on how to add custom user attributes later.

interface CommunicationObject {
    bpmId: string;
    // matrixId: string;
    // matrixHomeserverURL: string;
    // discordId: string;
}

interface ExampleUser {
    firstname: string,
    lastname: string,
    email: string,
    username: string,
    password: string,
    bpmIds: string[]
}

const example_users: ExampleUser[] = [
    {
        firstname: "John",
        lastname: "Doe",
        email: "john.doe@example.com",
        username: "johndoe123",
        password: "test",
        bpmIds: ["GE", "OX"]
    },
    {
        firstname: "Jane",
        lastname: "Smith",
        email: "jane.smith@example.com",
        username: "janesmith456",
        password: "test",
        bpmIds: ["BM", "NV"]
    },
    {
        firstname: "Gustav",
        lastname: "Eberhardt",
        email: "ge@s-bahn.de",
        username: "gustaverus",
        password: "test",
        bpmIds: ["GE"]
    },
    {
        firstname: "Otto",
        lastname: "Xander",
        email: "ox@s-bahn.de",
        username: "ottofant",
        password: "test",
        bpmIds: ["OX"]
    },
    {
        firstname: "Gisela",
        lastname: "Ulrich",
        email: "gisela.ulrich@abb.de",
        username: "dieechtegisela",
        password: "test",
        bpmIds: ["GU", "GiselaU"]
    },
    {
        firstname: "Viktor",
        lastname: "Stein",
        email: "vs@abb.de",
        username: "viktorinator",
        password: "test",
        bpmIds: ["VS"]
    },
    {
        firstname: "Bernd",
        lastname: "Müller",
        email: "bm@abb.de",
        username: "bernddasbrot",
        password: "test",
        bpmIds: ["BM"]
    },
    {
        firstname: "Nora",
        lastname: "Vogel",
        email: "nv@drossel-lieferant.de",
        username: "noranichtdora",
        password: "test",
        bpmIds: ["NV"]
    },
    {
        firstname: "Carla",
        lastname: "Neumann",
        email: "cn@drossel-lieferant.de",
        username: "karlakolumna",
        password: "test",
        bpmIds: ["CN"]
    },
    {
        firstname: "Paul",
        lastname: "Schmidt",
        email: "ps@drossel-lieferant.de",
        username: "dapauli",
        password: "test",
        bpmIds: ["PS"]
    },
    {
        firstname: "Igor",
        lastname: "Urban",
        email: "iu@ups.com",
        username: "igordergrosse",
        password: "test",
        bpmIds: ["IU","IgorU"]
    }
];

export default function Register(props: PageProps<Extract<KcContext, { pageId: "register.ftl" }>, I18n>) {
    const { kcContext, i18n, doUseDefaultCss, Template, classes } = props;
    const { getClassName } = useGetClassName({
        doUseDefaultCss,
        classes
    });

    const { url, messagesPerField, register, realm, passwordRequired, recaptchaRequired, recaptchaSiteKey } = kcContext;

    const { msg, msgStr } = i18n;


    const [communications, setCommunications] = useState<CommunicationObject[]>([]);

    const [currBpmId, setCurrBpmId] = useState('');
    const [invalidBpmId, setInvalidBpmId] = useState(false);

    // const [currMatrixId, setCurrMatrixId] = useState('');
    // const [invalidMatrixId, setInvalidMatrixId] = useState(false);

    // const [currMatrixHomeserverURL, setCurrMatrixHomeserverURL] = useState('');
    // const [invalidMatrixHomeserverURL, setInvalidMatrixHomeserverURL] = useState(false);

    // const [currDiscordId, setCurrDiscordId] = useState('');
    // const [invalidDiscordId, setInvalidDiscordId] = useState(false);


    const [hoverIndex, setHoverIndex] = useState(-999);

    // const [tabKey, setTabKey] = useState('matrix');

    // Add example users
    const addCommunicationDirectly = (commObj: CommunicationObject) => {
        const isUnique = !communications.some(comm => comm.bpmId === commObj.bpmId);
        if (isUnique && commObj.bpmId !== "") {
            setCommunications(prevComms => [...prevComms, commObj]);
        }
    };

    // Using document.get instead of state variables to not conflict with keycloak form
    const insertExampleUser = (index: number) => {
        setCommunications([]);
        example_users[index].bpmIds.forEach(bpmId => {
            addCommunicationDirectly({ bpmId: bpmId });
        });

        // Find each form element and check if it's not null before setting its value
        const firstNameElement = document.getElementById('firstName');
        if (firstNameElement instanceof HTMLInputElement) { // This check ensures that the element is not null and is an input element
            firstNameElement.value = example_users[index].firstname;
        }
        const lastNameElement = document.getElementById('lastName');
        if (lastNameElement instanceof HTMLInputElement) {
            lastNameElement.value = example_users[index].lastname;
        }
        const emailElement = document.getElementById('email');
        if (emailElement instanceof HTMLInputElement) {
            emailElement.value = example_users[index].email;
        }
        const usernameElement = document.getElementById('username');
        if (usernameElement instanceof HTMLInputElement) {
            usernameElement.value = example_users[index].username;
        }
        const passwordElement = document.getElementById('password');
        if (passwordElement instanceof HTMLInputElement) {
            passwordElement.value = example_users[index].password;
        }
        const passwordConfirmElement = document.getElementById('password-confirm');
        if (passwordConfirmElement instanceof HTMLInputElement) {
            passwordConfirmElement.value = example_users[index].password;
        }
    }

    // BPM IDS
    const handleBpmIdChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        checkForDuplicatesAndSetInvalid(event.target.value); // , currMatrixId, currDiscordId
        setCurrBpmId(event.target.value);
    };

    // // Matrix Communication
    // const handleMatrixIdChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    //     checkForDuplicatesAndSetInvalid(currBpmId, event.target.value, currDiscordId);
    //     setCurrMatrixId(event.target.value);

    // };
    // const handleMatrixHomeseverURLChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    //     setCurrMatrixHomeserverURL(event.target.value);
    // };

    // // Discord Communication
    // const handleDiscordIdChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    //     checkForDuplicatesAndSetInvalid(currBpmId, currMatrixId, event.target.value);
    //     setCurrDiscordId(event.target.value);
    // };

    const checkForDuplicatesAndSetInvalid = (newBpmID: string | undefined) => { // , newMatrixId: string | undefined, newDiscordId: string | undefined
        // Reset invalid states before the check
        setInvalidBpmId(false);
        // setInvalidMatrixId(false);
        // setInvalidDiscordId(false);

        communications.forEach((communication) => {
            if (newBpmID === communication.bpmId || communication.bpmId === "") {
                setInvalidBpmId(true);
            }
            // if (newMatrixId === communication.matrixId) {
            //     setInvalidMatrixId(true);
            // }
            // if (newDiscordId === communication.discordId) {
            //     setInvalidDiscordId(true);
            // }
        });
    };

    const handleRemoveCommunication = (index: number) => {
        setCommunications(communications.filter((_, i) => i !== index));
    };


    const addCommunication = () => {
        // Create a new object from the current input values
        const newCommunication: CommunicationObject = {
            bpmId: currBpmId,
            // matrixId: currMatrixId,
            // matrixHomeserverURL: currMatrixHomeserverURL,
            // discordId: currDiscordId,
        };

        // Check if the bpmId, matrixId, or discordId is unique in the array
        const isUnique = !communications.some(comm =>
            comm.bpmId === newCommunication.bpmId // ||
            // comm.matrixId === newCommunication.matrixId ||
            // comm.discordId === newCommunication.discordId
        );

        if (isUnique && currBpmId !== "") {
            // Add the new object to the array and reset the input fields
            setCommunications([...communications, newCommunication]);

            setCurrBpmId('');
            // setCurrMatrixId('');
            // setCurrMatrixHomeserverURL('');
            // setCurrDiscordId('');
        }
        if (currBpmId === "") {
            setInvalidBpmId(true);
        }
    };

    const [email, setEmail] = useState(register.formData.email ?? "");
    const [isValidEmail, setIsValidEmail] = useState(true);
    const [emailError, setEmailError] = useState('');

    const handleInvalid = (e: { preventDefault: () => void; }) => {
        e.preventDefault();
        setIsValidEmail(false);
        setEmailError("Please enter a valid email address.");
    };

    const handleEmailChange = (e: { target: { value: SetStateAction<string>; }; }) => {
        setEmail(e.target.value);
        if (!isValidEmail) {
            setIsValidEmail(true);
            setEmailError('');
        }
    };



    return (
        <Template {...{ kcContext, i18n, doUseDefaultCss, classes }} headerNode={msg("registerTitle")}>
            <Container style={{ margin: "0px", padding: "0px" }}>
                <Form id="kc-register-form" action={url.registrationAction} method="post"> {/*action={url.registrationAction} method="post" */}
                    <Form.Group controlId="firstName" style={fGroupStyle}>
                        <Form.Label>{msg("firstName")}</Form.Label>
                        <Form.Control type="text" name="firstName" defaultValue={register.formData.firstName ?? ""} className={messagesPerField.exists('firstName') ? 'is-invalid' : ''} />
                        {messagesPerField.exists('firstName') && (
                            <Form.Control.Feedback type="invalid">
                                {messagesPerField.get('lastName')}
                            </Form.Control.Feedback>
                        )}
                    </Form.Group>

                    <Form.Group controlId="lastName" style={fGroupStyle} className={messagesPerField.exists('lastName') ? 'is-invalid' : ''}>
                        <Form.Label>{msg("lastName")}</Form.Label>
                        <Form.Control
                            type="text"
                            name="lastName"
                            defaultValue={register.formData.lastName ?? ""}
                            isInvalid={messagesPerField.exists('lastName')}
                        />
                        {messagesPerField.exists('lastName') && (
                            <Form.Control.Feedback type="invalid">
                                {messagesPerField.get('lastName')}
                            </Form.Control.Feedback>
                        )}
                    </Form.Group>

                    <Form.Group controlId="email" className={messagesPerField.exists('email') ? 'is-invalid' : ''}>
                        <Form.Label>{msg("email")}</Form.Label>
                        <Form.Control
                            type="email"
                            name="email"
                            defaultValue={register.formData.email ?? ""}
                            autoComplete="email"
                            isInvalid={!isValidEmail || messagesPerField.exists('email')} // Uses the updated isValidEmail state
                            onChange={handleEmailChange}
                            onInvalid={handleInvalid}
                        />
                        {(!isValidEmail || messagesPerField.exists('email')) && (
                            <Form.Control.Feedback type="invalid">
                                {messagesPerField.exists('email') ? messagesPerField.get('email') : emailError}
                            </Form.Control.Feedback>
                        )}
                    </Form.Group>

                    {!realm.registrationEmailAsUsername && (
                        <Form.Group controlId="username" style={fGroupStyle} className={messagesPerField.exists('username') ? 'is-invalid' : ''}>
                            <Form.Label>{msg("username")}</Form.Label>
                            <Form.Control
                                type="text"
                                name="username"
                                defaultValue={register.formData.username ?? ""}
                                autoComplete="username"
                                isInvalid={messagesPerField.exists('username')}
                            />
                            {messagesPerField.exists('username') && (
                                <Form.Control.Feedback type="invalid">
                                    {messagesPerField.get('username')}
                                </Form.Control.Feedback>
                            )}
                        </Form.Group>
                    )}

                    {passwordRequired && (
                        <>
                            <Form.Group controlId="password" style={fGroupStyle} className={messagesPerField.exists('password') ? 'is-invalid' : ''}>
                                <Form.Label>{msg("password")}</Form.Label>
                                <Form.Control
                                    type="password"
                                    name="password"
                                    autoComplete="new-password"
                                    isInvalid={messagesPerField.exists('password')}
                                />
                                {messagesPerField.exists('password') && (
                                    <Form.Control.Feedback type="invalid">
                                        {messagesPerField.get('password')}
                                    </Form.Control.Feedback>
                                )}
                            </Form.Group>

                            <Form.Group controlId="password-confirm" style={fGroupStyle} className={messagesPerField.exists('password-confirm') ? 'is-invalid' : ''}>
                                <Form.Label>{msg("passwordConfirm")}</Form.Label>
                                <Form.Control
                                    type="password"
                                    name="password-confirm"
                                    isInvalid={messagesPerField.exists('password-confirm')}
                                />
                                {messagesPerField.exists('password-confirm') && (
                                    <Form.Control.Feedback type="invalid">
                                        {messagesPerField.get('password-confirm')}
                                    </Form.Control.Feedback>
                                )}
                            </Form.Group>
                        </>
                    )}

                    <Form.Group controlId="bpmids" style={fGroupStyle}>
                        <Form.Label>{msg("bpmLabel")}</Form.Label>
                        <Form.Control
                            as="textarea" // Makes it a textbox that can display multiple lines
                            name="user.attributes.accountDetails"
                            placeholder={msg("bpmLabel").props.children ? msg("bpmLabel").props.children : ""}
                            value={JSON.stringify(communications, null, 2)} // Format JSON for readability
                            readOnly={true}
                            style={{ ...fGroupStyle, height: '8em', cursor: 'default' }} // Adjust the style as needed
                        />
                    </Form.Group>

                    <Form.Group style={fGroupStyle}>
                        <Container>
                            <Row xs="auto">
                                {communications.map((communication, index) => (
                                    <Col key={index} style={{ padding: "0px 2px 0px 2px" }}>
                                        <Button
                                            className="d-flex align-items-center rounded-pill"
                                            variant="dark"
                                            style={{
                                                paddingTop: '0px',
                                                paddingBottom: '0px',
                                                paddingRight: '3px',
                                                paddingLeft: '5px',
                                                fontSize: '14px',
                                                border: '0px',
                                                color: 'black',
                                                ...(hoverIndex === index ? { backgroundColor: '#858a8d' } : { backgroundColor: 'rgb(230, 230, 230)' }),
                                            }}
                                            onMouseEnter={() => setHoverIndex(index)}
                                            onMouseLeave={() => setHoverIndex(-1)}
                                            onClick={() => handleRemoveCommunication(index)}
                                        >
                                            {communication.bpmId} <RiCloseLine size={20} color="#6d7173" style={{ marginLeft: "2px" }} />
                                        </Button>
                                    </Col>
                                ))}
                            </Row>
                        </Container>
                    </Form.Group>

                    <Container style={{ border: "2px solid #ccc8c8", borderRadius: "25px", marginTop: "25px", backgroundColor: "rgb(245, 240, 240)", paddingTop: "12px", paddingBottom: "10px" }}>
                        <Container>
                            <Row>
                                <Col style={{ padding: "0px" }}>
                                    <Form.Control style={fGroupStyle} placeholder={msg("bpmAddPlaceholder").props.children ? msg("bpmAddPlaceholder").props.children : ""} value={currBpmId} onChange={handleBpmIdChange} className={invalidBpmId ? 'is-invalid' : ''}></Form.Control>
                                </Col>
                            </Row>
                        </Container>
                        <Form.Group className="d-flex justify-content-center">
                            <Button className='btn-round' style={addConnector} onClick={addCommunication}>
                                <IoAddSharp style={addIcon} />
                            </Button>
                        </Form.Group>
                    </Container>
                    <Container style={{ margin: "0px", padding: "0px" }}>
                        <Row>
                            <Col>
                                <Dropdown >
                                    <Dropdown.Toggle style={{ margin: "0px", padding: "0px" }} variant="light">
                                        {msgStr("exampleUsers")}
                                    </Dropdown.Toggle>

                                    <Dropdown.Menu>
                                        {example_users.map((user, index) => (
                                            <Dropdown.Item key={index} onClick={() => { insertExampleUser(index) }} >
                                                {user.firstname} {user.lastname}
                                            </Dropdown.Item>
                                        ))}
                                    </Dropdown.Menu>
                                </Dropdown>
                            </Col>
                        </Row>
                    </Container>
                    <Container>
                        <Row>
                            <Col>
                                <Button
                                    variant="secondary"
                                    onClick={() => {
                                        window.location.href = url.loginUrl;
                                    }}
                                    style={loginButton}
                                >
                                    {msg("backToLogin")}
                                </Button>

                            </Col>

                            <Col>
                                <Button variant="primary" type="submit" style={loginButton}>
                                    {msgStr("doRegister")}
                                </Button>
                            </Col>
                        </Row>

                    </Container>

                </Form>
            </Container>
        </Template>
    );
}

const loginButton = {
    width: '100%',
    marginBottom: 25,
    marginTop: 25
}

const fGroupStyle = {
    marginTop: '10px'
}

const addConnector = {
    marginTop: '10px',
    scale: '80%'
}

const addIcon = {
    scale: '180%',
    position: 'relative' as const,
    bottom: 2,
    right: 2
}
