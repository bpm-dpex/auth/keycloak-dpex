import { createUseI18n } from "keycloakify/login";

export const { useI18n } = createUseI18n({
    en: {
        alphanumericalCharsOnly: "Only alphanumerical characters",
        gender: "Gender",
        doForgotPassword: "I forgot my password",
        invalidUserMessage: "Invalid username or password.",
        bpmLabel: "Identities that will be saved to your account",
        bpmAddPlaceholder: "BPM ID to add...",
        communicationAccounts: "Accounts for Communication",
        communicationIdM: "Account ID on Matrix",
        homeserverUrlM: "The Homeserver URL of your Matrix Account",
        communicationIdD: "Account ID on Discord",
        exampleUsers: "Example Users"
    },
    fr: {
        alphanumericalCharsOnly: "Caractère alphanumérique uniquement",
        gender: "Genre",
        doForgotPassword: "J'ai oublié mon mot de passe",
        invalidUserMessage: "Nom d'utilisateur ou mot de passe invalide.",
        bpmLabel: "Identités qui seront enregistrées sur votre compte",
        bpmAddPlaceholder: "ID BPM à ajouter...",
        communicationAccounts: "Comptes de Communication",
        communicationIdM: "ID du compte sur Matrix",
        homeserverUrlM: "URL du serveur domestique de votre compte Matrix",
        communicationIdD: "ID du compte sur Discord",
        exampleUsers: "Utilisateurs Exemples"
    },
    de: {
        alphanumericalCharsOnly: "Nur alphanumerische Zeichen",
        gender: "Geschlecht",
        doForgotPassword: "Ich habe mein Passwort vergessen",
        invalidUserMessage: "Ungültiger Benutzername oder Passwort.",
        bpmLabel: "Identitäten, die in Ihrem Konto gespeichert werden",
        bpmAddPlaceholder: "BPM-ID hinzufügen...",
        communicationAccounts: "Kommunikationskonten",
        communicationIdM: "Kontoidentifikator auf Matrix",
        homeserverUrlM: "Die Homeserver-URL Ihres Matrix-Kontos",
        communicationIdD: "Kontoidentifikator auf Discord",
        exampleUsers: "Beispielnutzer"
    },

    jp: {
        alphanumericalCharsOnly: "英数字のみ",
        gender: "性別",
        doForgotPassword: "パスワードを忘れた場合",
        invalidUserMessage: "無効なユーザー名またはパスワードです。",
        bpmLabel: "あなたのアカウントに保存されるアイデンティティ",
        bpmAddPlaceholder: "追加するBPM ID...",
        communicationAccounts: "コミュニケーション用アカウント",
        communicationIdM: "MatrixアカウントのID",
        homeserverUrlM: "あなたのMatrixアカウントのホームサーバーURL",
        communicationIdD: "DiscordアカウントのID",
        exampleUsers: "例のユーザー"
    },
    it: {
        alphanumericalCharsOnly: "Solo caratteri alfanumerici",
        gender: "Genere",
        doForgotPassword: "Ho dimenticato la mia password",
        invalidUserMessage: "Nome utente o password non validi.",
        bpmLabel: "Identità che saranno salvate sul tuo account",
        bpmAddPlaceholder: "ID BPM da aggiungere...",
        communicationAccounts: "Account per la comunicazione",
        communicationIdM: "ID dell'account su Matrix",
        homeserverUrlM: "L'URL del server domestico del tuo account Matrix",
        communicationIdD: "ID dell'account su Discord",
        exampleUsers: "Utenti Esempio"
    },
    es: {
        alphanumericalCharsOnly: "Solo caracteres alfanuméricos",
        gender: "Género",
        doForgotPassword: "Olvidé mi contraseña",
        invalidUserMessage: "Nombre de usuario o contraseña no válidos.",
        bpmLabel: "Identidades que se guardarán en tu cuenta",
        bpmAddPlaceholder: "ID BPM para agregar...",
        communicationAccounts: "Cuentas para comunicación",
        communicationIdM: "ID de cuenta en Matrix",
        homeserverUrlM: "La URL del servidor de inicio de sesión de tu cuenta de Matrix",
        communicationIdD: "ID de cuenta en Discord",
        exampleUsers: "Usuarios Ejemplo"
    },
});

export type I18n = NonNullable<ReturnType<typeof useI18n>>;
