import { useState } from "react";
import { assert } from "keycloakify/tools/assert";
import { usePrepareTemplate } from "keycloakify/lib/usePrepareTemplate";
import { type TemplateProps } from "keycloakify/login/TemplateProps";
import { useGetClassName } from "keycloakify/login/lib/useGetClassName";
import type { KcContext } from "./kcContext";
import type { I18n } from "./i18n";
import keycloakifyLogoPngUrl from "./assets/dpex-logo-full.png";
import { Card, Dropdown, Button, Form, Container, Col, Row, Alert, OverlayTrigger, Tooltip } from 'react-bootstrap';
import NavBar from "keycloak-theme/customcomponents/NavBar";
import Footer from "keycloak-theme/customcomponents/Footer";
import { IoLanguageOutline } from "react-icons/io5";
import { FaArrowRotateLeft } from "react-icons/fa6";
import { AiOutlineInfoCircle, AiOutlineCloseCircle, AiOutlineCheckCircle, AiOutlineExclamationCircle } from "react-icons/ai";
import React from "react";

export default function Template(props: TemplateProps<KcContext, I18n>) {
    const {
        displayInfo = false,
        displayMessage = true,
        displayRequiredFields = false,
        displayWide = false,
        showAnotherWayIfPresent = true,
        headerNode,
        showUsernameNode = null,
        infoNode = null,
        kcContext,
        i18n,
        doUseDefaultCss,
        classes,
        children
    } = props;

    const { getClassName } = useGetClassName({ doUseDefaultCss, classes });

    const { msg, changeLocale, labelBySupportedLanguageTag, currentLanguageTag } = i18n;

    const { realm, locale, auth, url, message, isAppInitiatedAction } = kcContext;


    useState(() => { document.title = i18n.msgStr("loginTitle", kcContext.realm.displayName); });


    const { isReady } = usePrepareTemplate({
        "doFetchDefaultThemeResources": doUseDefaultCss,
        "styles": [
            `${url.resourcesCommonPath}/node_modules/patternfly/dist/css/patternfly.min.css`,
            `${url.resourcesCommonPath}/node_modules/patternfly/dist/css/patternfly-additions.min.css`,
            `${url.resourcesCommonPath}/lib/zocial/zocial.css`,
            `${url.resourcesPath}/css/login.css`
        ],
        "htmlClassName": getClassName("kcHtmlClass"),
        "bodyClassName": getClassName("kcBodyClass")
    });


    // My functions
    // Function to handle form submission
    const handleSubmitTryAnotherWay = (e: { preventDefault: () => void; currentTarget: any; }) => {
        e.preventDefault(); // Prevent default form submission behavior
        const form = e.currentTarget;
        form.submit();
    };

    const alertMessage = () => {
        // App-initiated actions should not see warning messages about the need to complete the action during login.
        return (
            displayMessage && message !== undefined && (message.type !== "warning" || !isAppInitiatedAction) && (
                <Alert className="d-flex align-items-center" variant={message.type === "error" ? "danger" : message.type} style={{ marginTop: "10px" }}>
                    {message.type === "warning" &&
                        <AiOutlineExclamationCircle size={30} style={{ marginRight: "10px" }} />
                    }
                    {message.type === "success" &&
                        <AiOutlineCheckCircle size={30} style={{ marginRight: "10px" }} />
                    }
                    {message.type === "error" &&
                        <AiOutlineCloseCircle size={30} style={{ marginRight: "10px" }} />
                    }
                    {message.type === "info" &&
                        <AiOutlineInfoCircle size={30} style={{ marginRight: "10px" }} />
                    }
                    {message.summary}
                </Alert>
            )
        );
    };


    if (!isReady) {
        return null;
    }


    // If SSI with Google or other providers should be supported later the displayWide state can be used
    return (
        <>
            <NavBar {...url} />

            <Container style={loginContainer}>
                <Card style={loginCard}>
                    <Container>
                        <Row>
                            <Col className="d-flex justify-content-end align-items-center" style={loginCol}>
                                {realm.internationalizationEnabled && (assert(locale !== undefined), true) && locale.supported.length > 1 && (
                                    <React.Fragment>

                                        <Dropdown id="kc-locale">
                                            <Dropdown.Toggle id="dropdown-basic" variant="light">
                                                {labelBySupportedLanguageTag[currentLanguageTag]}
                                            </Dropdown.Toggle>

                                            <Dropdown.Menu>
                                                {locale.supported.map(({ languageTag }) => (
                                                    <Dropdown.Item
                                                        key={languageTag}
                                                        onClick={() => changeLocale(languageTag)}
                                                    >
                                                        {labelBySupportedLanguageTag[languageTag]}
                                                    </Dropdown.Item>
                                                ))}
                                            </Dropdown.Menu>
                                        </Dropdown>
                                        <IoLanguageOutline size={30} color="#626569"></IoLanguageOutline>
                                    </React.Fragment>
                                )}
                            </Col>
                        </Row>
                        <Row>
                            <Col className="d-flex justify-content-center">
                                <Card.Img src={keycloakifyLogoPngUrl} style={dpexLogoStyle} />
                            </Col>
                        </Row>
                        {!(auth !== undefined && auth.showUsername && !auth.showResetCredentials) ? (
                            displayRequiredFields ? (
                                <Row>
                                    <Col>
                                        <Card.Title style={titleStyle}>{headerNode}</Card.Title>
                                        {alertMessage()}
                                        <Card.Subtitle>{`*${msg("requiredFields")}`}</Card.Subtitle>
                                    </Col>
                                </Row>
                            ) : (
                                <Row>
                                    <Col>
                                        <Card.Title style={titleStyle}>{headerNode}</Card.Title>
                                        {alertMessage()}
                                    </Col>
                                </Row>
                            )
                        ) : displayRequiredFields ? (
                            <>
                                <Row>
                                    <Col>
                                        <Card.Title style={titleStyle}>{headerNode}</Card.Title>
                                        {alertMessage()}
                                        <Card.Subtitle>{`*${msg("requiredFields")}`}</Card.Subtitle>
                                    </Col>
                                </Row>
                                <Row>
                                    <Form.Group className="mb-3" controlId="formBasicEmail">
                                        <Container>
                                            {showUsernameNode}
                                            <Row>
                                                <Col className="d-flex align-items-center" style={{ paddingLeft: "0px", paddingRight: "0px" }}>
                                                    <Form.Control type="email" value={auth?.attemptedUsername} disabled />
                                                </Col>
                                                <Col className="d-flex align-items-center" md="auto" style={{}}>
                                                    <OverlayTrigger
                                                        overlay={<Tooltip id="tooltip-reset-email">{msg("restartLoginTooltip")}</Tooltip>}
                                                        placement="top"
                                                    >
                                                        <a id="reset-login" href={url.loginRestartFlowUrl}>
                                                            <span>
                                                                <FaArrowRotateLeft size={25} color="#768088" />
                                                            </span>
                                                        </a>
                                                    </OverlayTrigger>
                                                </Col>
                                            </Row>
                                        </Container>
                                    </Form.Group>
                                </Row>
                            </>


                        ) : (
                            <>
                                <Row>
                                    <Col>
                                        <Card.Title style={titleStyle}>{headerNode}</Card.Title>
                                        {alertMessage()}
                                    </Col>
                                </Row>
                                {showUsernameNode}
                                <Row>
                                    <Form.Group className="mb-3" controlId="formBasicEmail">
                                        <Container>
                                            {showUsernameNode}
                                            <Row>
                                                <Col className="d-flex align-items-center" style={{ paddingLeft: "0px", paddingRight: "0px" }}>
                                                    <Form.Control type="email" value={auth?.attemptedUsername} disabled />
                                                </Col>
                                                <Col className="d-flex align-items-center" md="auto" style={{}}>
                                                    <OverlayTrigger
                                                        overlay={<Tooltip id="tooltip-reset-email">{msg("restartLoginTooltip")}</Tooltip>}
                                                        placement="top"
                                                    >
                                                        <a id="reset-login" href={url.loginRestartFlowUrl}>
                                                            <span>
                                                                <FaArrowRotateLeft size={25} color="#768088" />
                                                            </span>
                                                        </a>
                                                    </OverlayTrigger>
                                                </Col>
                                            </Row>
                                        </Container>
                                    </Form.Group>
                                </Row>
                            </>
                        )}

                        {children}

                        {auth !== undefined && auth.showTryAnotherWayLink && showAnotherWayIfPresent && (
                            <Row>
                                <Col>
                                    <Form
                                        id="kc-select-try-another-way-form"
                                        action={url.loginAction}
                                        method="post"
                                        onSubmit={handleSubmitTryAnotherWay}
                                    >
                                        <input type="hidden" name="tryAnotherWay" value="on" />
                                        <Button
                                            variant="link"
                                            type="submit"
                                            id="try-another-way"
                                        >
                                            {msg("doTryAnotherWay")}
                                        </Button>
                                    </Form>
                                </Col>
                            </Row>
                        )}
                        {/* Display info from original login that would display register field has to be handled by Login.tsx in this design */}
                        {displayInfo &&
                            <Row>
                                <Col>
                                    {infoNode}
                                </Col>
                            </Row>
                        }
                    </Container>
                </Card>
            </Container>
            <Footer />
        </>
    );
}


const loginCol = {
    margin: 0,
    padding: 0
}

const loginContainer = {
    paddingTop: '3rem',
    justifyContent: 'center',
    display: 'flex'
}

const loginCard = {
    paddingTop: '1rem',
    paddingLeft: '1rem',
    paddingRight: '1rem',
    width: '40rem',
    borderRadius: 15,
    background: 'snow'
}

const dpexLogoStyle = {
    maxWidth: '20rem',
    margin: '0 auto'
}

const loginButton = {
    width: '100%',
    marginBottom: 25
}

const internationalizationButton = {
    backgroundColor: '#fffafa'
};

const titleStyle = {
    textAlign: 'center' as const,
    marginTop: 20,
    fontSize: 25
}